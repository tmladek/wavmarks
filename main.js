const dropElement = document.getElementById("dropmark");
const label = document.getElementById("label");
const infoElement = document.getElementById("cuelist-outer");
const cueListElement = document.getElementById("cuelist");
const exportButton = document.getElementById("export-button");


function processFile (file) {
    window.currentFile = file;
    console.debug(window.currentFile);
    file.arrayBuffer().then((arrayBuffer) => {
        const buffer = new Uint8Array(arrayBuffer);
        window.resultWav = new wavefile.WaveFile(buffer);
        console.debug(window.resultWav);
        window.cuePoints = window.resultWav.listCuePoints().map((point) => {
            return new Date(point.position).toISOString().substr(11, 12);
        });
        console.debug(window.cuePoints);
        displayMarks();
    });
}

function displayMarks () {
    const length = window.resultWav.data.chunkSize / window.resultWav.fmt.byteRate;
    label.innerText = `${window.currentFile.name}: ${length.toFixed(2)}s`;

    while (cueListElement.lastElementChild) {
        cueListElement.removeChild(cueListElement.lastElementChild);
    }

    window.cuePoints.forEach((point) => {
        const row = document.createElement("li");
        row.innerText = point;
        cueListElement.appendChild(row);
    });

    infoElement.style.visibility = "inherit";
    exportButton.disabled = false;
}

function exportAsTSV () {
    const filename = `${window.currentFile.name.replace(/\.(wav|WAV)$/, "")}_marks.csv`;
    let text = "Name\tStart\tDuration\tTime Format\tType\tDescription\n";
    window.cuePoints.forEach((point, idx) => {
        text += `Mark ${idx}\t${point}\t0:00.000\tdecimal\tCue\n`;
    });

    console.debug(text);

    const aElement = document.createElement("a");
    aElement.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    aElement.setAttribute("download", filename);
    aElement.click();
}

function handleFileSelect (evt) {
    evt.stopPropagation();
    evt.preventDefault();

    processFile(evt.dataTransfer.files.item(0));
}

function handleDragOver (evt) {
    evt.stopPropagation();
    evt.preventDefault();
}

function handleClick (evt) {
    evt.stopPropagation();
    evt.preventDefault();

    const fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.accept = ".wav";
    fileInput.onchange = () => {
        if (fileInput.files !== null) {
            processFile(fileInput.files.item(0));
        }
    };
    fileInput.click();
}

dropElement.addEventListener("dragover", handleDragOver, false);
dropElement.addEventListener("drop", handleFileSelect, false);
dropElement.addEventListener("click", handleClick, false);
exportButton.addEventListener("click", exportAsTSV, false);
